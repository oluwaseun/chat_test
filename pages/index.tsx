// import styles from "../styles/Home.module.css";
import Link from "next/link";

export default function Home() {
  return (
    <div className="flex flex-col h-screen items-center justify-center space-y-10">
      <section>
        <Link href="/login">
          <a className="p-3 bg-blue-100">Try our Beta for Free</a>
        </Link>
      </section>
      <section>
        <h1>Whatsapp Clone Project By Seun</h1>
        <h5>Maybe even Mindpulse</h5>
        <p>Landing Page</p>
      </section>
    </div>
  );
}
