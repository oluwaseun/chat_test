import { useState } from "react";
import { useLoginHandling } from "../hooks/auth/useLoginHandling";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { handleLogin: afterlogin, loading, error } = useLoginHandling();

  const handleLogin = (e: React.FormEvent) => {
	e.preventDefault();
    if (typeof window !== "undefined") {
      afterlogin(email, password);
      // dispatch(login(email, password));
    }
  };

  return (
    <div className="bg-blue-100 bg-opacity-60 h-screen flex items-center">
      <div className="m-auto p-0 pt-10 w-1/4 bg-blue-100 text-gray-800 text-sm">
        <h1 className="text-xl text-center">Login</h1>
        {error && (
          <h3 className="bg-red-200 text-center p-2 mt-1">
            Login Failed. {`${error}`}
          </h3>
        )}
        <form
          id="login-form"
          onSubmit={handleLogin}
          className="flex flex-col space-y-6 m-4 bg-blue-100 p-4"
        >
          <div className="flex flex-col space-y-2">
            <label htmlFor="email">Email</label>
            <input
              id="email"
              type="email"
              value={email}
              placeholder="Enter your email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="flex flex-col space-y-2">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              value={password}
              placeholder="Enter your password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <input type="submit" value="Login" className="bg-blue-300" />
        </form>
      </div>
    </div>
  );
}
