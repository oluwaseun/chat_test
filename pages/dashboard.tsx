import Link from "next/link";
import Layout from "../components/Layout";
import { useCurrentUser } from "../hooks/users";

export default function Dashboard() {
  const { user } = useCurrentUser();
  return (
    <Layout>
      <div>Welcome, {user?.firstname}</div>
      <div className="flex flex-col">
        <Link href="/chat">
          <a className="bg-blue-100 btn">Chat with a Therapist</a>
        </Link>
        <Link href="#">
          <a className="bg-blue-100 btn">Start a video session</a>
        </Link>
      </div>
    </Layout>
  );
}
