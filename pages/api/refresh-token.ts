import type { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";
import { getCookie, serializeCookie } from "../../shared/cookieUtils";
import { apiConfig } from "../../config/api";

export default async function tokenRefresh(
    req: NextApiRequest,
    res: NextApiResponse
) {
    try {
        const currRefreshToken = req.headers.cookie
            ? getCookie(req.headers.cookie)
            : "";

        const { accessToken, refreshToken } = await axios
            .post(`${apiConfig.baseUrl}/users/refresh-token`, {
                refreshToken: currRefreshToken,
            })
            .then((resp) => resp.data.data);

        if (!refreshToken) {
            throw new Error("No secret present in login query response.");
        }
        const cookieSerialized = serializeCookie(refreshToken);

        res.setHeader("Set-Cookie", cookieSerialized);
        res.status(200).json({ accessToken });
    } catch (error) {
        res.status(400).send(error.message);
    }
}
