import type { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";
import { serializeCookie } from "../../shared/cookieUtils";
import { apiConfig } from "../../config/api";

export default async function login(req: NextApiRequest, res: NextApiResponse) {
  const { email, password } = await req.body;

  try {
    if (!email || !password) {
      throw new Error("Email and password must be provided.");
    }

    const {
      accessToken,
      refreshToken,
    } = await axios
      .post(`${apiConfig.baseUrl}/users/login`, { email, password })
      .then((resp) => resp.data.data);

    if (!refreshToken) {
      throw new Error("No secret present in login query response.");
    }
    const cookieSerialized = serializeCookie(refreshToken);

    res.setHeader("Set-Cookie", cookieSerialized);
    res.status(200).json({ accessToken });
  } catch (error) {
    console.log(error);
    //TODO Return exact error code sent from server
    res.status(400).send(error.message);
  }
}
