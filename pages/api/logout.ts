import type { NextApiRequest, NextApiResponse } from "next";
import { serializeExpiredCookie } from "../../shared/cookieUtils";

export default async function logout(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    // set cookie to empty
    const expiredCookieSerialized = serializeExpiredCookie();
    res.setHeader("Set-Cookie", expiredCookieSerialized);
    return res.end();
  } catch (error) {
    console.error(error);
    res.status(error.status || 500).end(error.message);
  }
}
