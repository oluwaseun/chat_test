import { SearchableRoomsList } from "../components/Chat/SearchableChatRoomsList";
import { SideBarHeader } from "../components/Chat/SideBarHeader";
import { ChatRoomInterface } from "../components/Chat/ChatRoomInterface";
import React, { useState } from "react";
import { RightSideBar } from "../components/Chat/RightSideBar";
import { LeftSideBar } from "../components/Chat/LeftSideBar";
import Layout from "../components/Layout";
import { ChatRoom, useChatUser } from "../hooks/chat";
import { ChatPasswordForm } from "../components/Chat/ChatPassword";
import { UserRole } from "../hooks/users";

export default function Chat() {
  const { chatUser, loading, error } = useChatUser();

  const [selectedChatRoom, setSelectedChatRoom] = useState<ChatRoom>();
  const [hideRightSidebar, setHideRightSidebar] = useState(true);
  const [keyPair, setKeyPair] = useState<CryptoKeyPair>();

  const onChatroomClick = (chatRoom: ChatRoom) => {
    setSelectedChatRoom(chatRoom);
  };

  const onContactClick = () => {
    setHideRightSidebar(!hideRightSidebar);
  };

  const onKeyPairDecryption = (dKeyPair: CryptoKeyPair) => {
    setKeyPair(dKeyPair);
    if (chatUser.role !== UserRole.Therapist)
      setSelectedChatRoom(chatUser.rooms[0]);
  };

  return (
    <Layout>
      <div className="h-screen w-full overflow-hidden py-6">
        {error && "Oops, Something went wrong, Please try again."}
        {loading && "Sigh, Let's wait a lil bit more..."}
        {/* MainWrapper */}
        {chatUser && (
          <div className="xl:container mx-auto flex h-full">
            {/* ProfileDrawer */}
            {!keyPair && (
              <ChatPasswordForm
                eKeyPair={{
                  ePrivateKey: chatUser.ePrivateKey || "",
                  publicKeyPem: chatUser.publicKey || "",
                }}
                onKeyPairDecryption={onKeyPairDecryption}
              />
            )}
            {keyPair && (
              <>
                {chatUser.role === UserRole.Therapist && (
                  <LeftSideBar>
                    <SideBarHeader />
                    <SearchableRoomsList
                      rooms={chatUser?.rooms || []}
                      onChatRoomClick={onChatroomClick}
                    />
                  </LeftSideBar>
                )}
                {selectedChatRoom && (
                  <ChatRoomInterface
                    keyPair={keyPair}
                    chatRoom={selectedChatRoom}
                    chatUser={chatUser}
                    onContactClick={onContactClick}
                  />
                )}
                {/* ContactDrawer, SearchMessagesDrawer */}
                <RightSideBar hide={hideRightSidebar}>
                  <div>Right SideBar</div>
                </RightSideBar>
              </>
            )}
          </div>
        )}
      </div>
    </Layout>
  );
}

// {/* <img src={"images/whatsapp-main.png"} /> */}
// MainWrapper
// 	ProfileDrawer
// 	SideBar
// 		SideBarHeader
// 			ProfileImage
// 			StatusButton
// 			NewChatButton
// 			SideBarMenu
// 		SearchChaRoomsInput
// 		ChatChatRoomsList
// 			ChatRoom {displayPicture, chatName, lastMessage, lastMessageTime, unreadMessages}
// 	ChatRoomInterface
// 		ChatHeader
// 			Contact { displayPicture, chatName, isOnline, lastSeen }
// 			SearchMessagesButton
//			ChatMenu
//		ChatWindow
//			MyMessages
//			OtherMessages
// 		ChatFooter
// 			EmojiButton
// 			AttachButton
// 			AttachMenu
// 			MessageInput
// 			VoiceMessageButton
// 		EmojiDrawer
// 	ContactDrawer
//	SearchMessagesDrawer
