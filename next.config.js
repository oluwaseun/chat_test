const withPWA = require("next-pwa");
const withPlugins = require("next-compose-plugins");
const runtimeCaching = require("next-pwa/cache");
const withTM = require("next-transpile-modules")(["opencrypto"]);

module.exports = withPlugins([withTM, withPWA], {
  pwa: {
    disable: process.env.NODE_ENV === "development",
    dest: "public",
    runtimeCaching,
  },
});
