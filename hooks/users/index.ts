import useSWR from "swr";
import { fetcher } from "../../shared/services/api";

/*
interface IUserService {
    signup(email: string, password: string): Promise<APIResponse<void>>;
    sendPasswordResetMail(email: string): Promise<APIResponse<void>>;
    confirmPasswordReset(
        userId: string,
        password: string,
        token: string
    ): Promise<APIResponse<void>>;
    confirmEmail(token: string): Promise<APIResponse<void>>;
}
*/
export enum UserRole {
    SuperAdmin = "SUPER_ADMIN",
    MindpulseAdmin = "MINDPULSE_ADMIN",
    MindpulsePatient = "MINDPULSE_PATIENT",
    OrganizationAdmin = "ORGANIZATION_ADMIN",
    OrganizationStaff = "ORGANIZATION_STAFF",
    Therapist = "THERAPIST",
}

export type UserProfile = {
    email: string;
    userId: string;
    role: UserRole;
    isEmailVerified: boolean;
    organizationId: string;
    firstname?: string;
    lastname?: string;
    userState: string;
    lastLoginIp?: string;
    lastLoginTime?: string;
    createdAt?: string;
    updatedAt?: string;
    displayPicture?: string;
};

export const useCurrentUser = () => {
    const { data, error } = useSWR("/users/me", fetcher, {
        revalidateOnReconnect: false,
    });
    const user = data?.user as UserProfile;

    return {
        user,
        loading: !error && !data,
        error,
    };
};
