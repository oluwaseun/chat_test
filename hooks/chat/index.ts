import { useEffect } from "react";
import useSWR, { mutate } from "swr";
import { fetcher, useAxios } from "../../shared/services/api";
import { UserRole } from "../users";

export type ChatUser = {
    chatUserId: string;
    userId: string;
    role: UserRole;
    firstname?: string;
    lastname?: string;
    publicKey?: string;
    ePrivateKey?: string;
    rooms: ChatRoom[];
};
export type ChatRoom = {
    roomId: string;
    name: string;
    users: ChatRoomUser[];
    lastMessage?: ChatMessageData;
};

export type ChatRoomUser = {
    chatUserId: string;
    publicKey: string;
    name: string | null;
};

export type ChatMessageData = {
    id: string;
    messageType?: string;
    eMessage: string;
    sentAt: string;
    chatRoomId: string;
    senderId: string;
    userData: ChatUserData[];
};

export type DecryptedChatMessageData = ChatMessageData & {
    value: string;
    fromMe: boolean;
};

export type ChatUserData = {
    chatUserId: string;
    eMKey: string;
    isRead: boolean;
};

const MY_CHAT_USER = "/chat-users/me";
const CHAT_ROOMS = "/chat-rooms";

/* Fetch Chat User */
export const useChatUser = () => {
    const { data, error } = useSWR(MY_CHAT_USER, fetcher);
    const chatUser = data?.chatUser as ChatUser;

    return {
        chatUser,
        loading: !error && !data,
        error,
    };
};

/* Fetch Room Messages*/
export const useChatRoomMessages = (
    roomId: string,
    page: number = 0,
    perPage: number = 20
) => {
    const { data, error } = useSWR(
        `${CHAT_ROOMS}/${roomId}/messages/?page=${page}&perPage=${perPage}`,
        fetcher,
        {
            revalidateOnFocus: false,
            revalidateOnReconnect: false,
        }
    );
    const eMessages = data?.messages as ChatMessageData[];

    return {
        eMessages,
        loading: !error && !data,
        error,
    };
};

/* Update ChatUser keys*/
export const useUpdateChatUserKeys = () => {
    const [{ data, loading, error }, execute] = useAxios(
        {
            url: MY_CHAT_USER,
            method: "PATCH",
        },
        { manual: true }
    );

    useEffect(() => {
        mutate(MY_CHAT_USER);
    }, [data]);

    return {
        data,
        loading,
        error,
        updateChatUserKeys: (publicKey: string, privateKey: string) => {
            return execute({ data: { publicKey, privateKey } });
        },
    };
};
