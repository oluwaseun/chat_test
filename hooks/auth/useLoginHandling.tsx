import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { authService } from "../../shared/services/authService";

export const useLoginHandling = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const router = useRouter();

  const handleLoggedIn = () => {
    const { redirect } = router.query;
    console.log("redirecting... ");
    redirect ? router.back() : router.push("/dashboard");
  };

  useEffect(() => {
    if (authService.isAuthenticated()) {
      handleLoggedIn();
    }
  });

  const handleLogin = async (email: string, password: string) => {
    setLoading(true);
    const { errorMessage } = await authService.login(email, password);
    if (errorMessage) {
      setError(errorMessage);
    } else {
      handleLoggedIn();
      setLoading(false);
    }
  };

  const loginHandlingProps = {
    handleLogin,
    loading,
    error,
  };

  return loginHandlingProps;
};
