const isProduction = process.env.NODE_ENV === "production";

const devApiConfig = {
  baseUrl: "http://localhost:4000/v1",
};

const prodApiConfig = {
  baseUrl: "https://api.mindpulse.ng/v1",
};

const apiConfig = isProduction ? prodApiConfig : devApiConfig;

export { apiConfig };
