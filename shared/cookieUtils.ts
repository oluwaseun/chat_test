import cookie from "cookie";

export const REFRESH_TOKEN_NAME = "refresh-token";

export const serializeCookie = (
  value: string,
  name: string = REFRESH_TOKEN_NAME
) => {
  const cookieSerialized = cookie.serialize(name, value, {
    sameSite: "lax",
    secure: process.env.NODE_ENV === "production",
    maxAge: 72576000,
    httpOnly: true,
    path: "/",
  });
  return cookieSerialized;
};

export const serializeExpiredCookie = (name: string = REFRESH_TOKEN_NAME) => {
  const cookieSerialized = cookie.serialize(name, "", {
    sameSite: "lax",
    secure: process.env.NODE_ENV === "production",
    maxAge: -1,
    httpOnly: true,
    path: "/",
  });
  return cookieSerialized;
};

export const getCookie = (
  cookieString: string,
  name: string = REFRESH_TOKEN_NAME
) => {
  const cookies = cookie.parse(cookieString);
  return cookies[name];
};
