// import OpenCrypto from "opencrypto";
// const crypt = typeof window !== "undefined" ? new OpenCrypto() : {};
let crypt: any = {};
if (typeof window !== "undefined") {
    import("opencrypto").then((opencrypto) => {
        const OpenCrypto = opencrypto.default;
        crypt = new OpenCrypto();
    });
}

/* Export an extractable key */
async function exportCryptoKey(key: CryptoKey) {
    const exported = await crypto.subtle.exportKey("jwk", key);
    return JSON.stringify(exported);
}

/* Convert a string to UInt8Array */
function getMessageEncoding(message: string) {
    let enc = new TextEncoder();
    return enc.encode(message);
}

/* Convert  an ArrayBuffer into a string */
function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}

export async function generateKeyPair(): Promise<CryptoKeyPair> {
    return await crypt.getRSAKeyPair(2048, "SHA-256");
}

export async function encryptPrivateKey(
    privateKey: CryptoKey,
    password: string
): Promise<string> {
    return await crypt.encryptPrivateKey(privateKey, password);
}

export async function publicKeyToPem(publicKey: CryptoKey): Promise<string> {
    return await crypt.cryptoPublicToPem(publicKey);
}
export async function pemToPublicKey(publicKeyPem: string): Promise<CryptoKey> {
    return await crypt.pemPublicToCrypto(publicKeyPem);
}

export async function decryptPrivateKey(
    encryptedPrivateKey: string,
    password: string
): Promise<CryptoKey> {
    return await crypt.decryptPrivateKey(encryptedPrivateKey, password, {
        name: "RSA-OAEP",
        hash: "SHA-512",
        usages: ["decrypt", "unwrapKey"],
        isExtractable: true,
    });
}

export async function generateMessageEncryptionKey(): Promise<CryptoKey> {
    return await crypt.getSharedKey(128);
}

export async function encryptMessage(
    message: string,
    aesKey: CryptoKey
): Promise<string> {
    const msgBuf = getMessageEncoding(message);
    return await crypt.encrypt(aesKey, msgBuf);
}

export async function decryptMessage(
    aesKey: CryptoKey,
    encryptedMessage: string
) {
    const decryptedMessageBuf = await crypt.decrypt(aesKey, encryptedMessage);
    return ab2str(decryptedMessageBuf);
}

export async function encryptMessageKey(
    publicKey: CryptoKey,
    aesKey: CryptoKey
): Promise<string> {
    return await crypt.encryptKey(publicKey, aesKey);
}

export async function decryptMessageKey(
    privateKey: CryptoKey,
    encryptedAesKey: string
): Promise<CryptoKey> {
    return await crypt.decryptKey(privateKey, encryptedAesKey);
}
