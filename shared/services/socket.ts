import { io, Socket } from "socket.io-client";
import { ChatMessageData } from "../../hooks/chat";

let socket: Socket;

export enum ChatEvent {
    Connection = "connection",
    JoinRoom = "join_room",
    LeftRoom = "left_room",
    UserJoined = "user_joined",
    UserTyping = "user_started_typing",
    UserStoppedTyping = "user_stopped_typing",
    NewMessage = "new_message",
    MessageSaved = "message_saved",
    MessageRead = "message_read",
    Disconnect = "disconnect",
}

export const initiateSocket = (roomId: string, userId: string) => {
    socket = io("http://localhost:4000/chats");
    console.log(`Connecting socket...`);
    console.log(">>>> Joining room", roomId);
    if (socket && roomId) socket.emit(ChatEvent.JoinRoom, { roomId, userId });
};

export const disconnectSocket = () => {
    console.log("Disconnecting socket...");
    if (socket) socket.disconnect();
};

export const sendMessage = (message: ChatMessageData) => {
    if (socket) socket.emit(ChatEvent.NewMessage, message);
};

export const userTyping = (roomId: string, userId: string) => {
    if (socket) socket.emit(ChatEvent.UserTyping, { roomId, userId });
};

export const subscribeToChatSocket = (
    cb: (err: Error | null, event: ChatEvent, data: any) => void
) => {
    if (!socket) return true;
    socket.onAny((eventName, args) => {
        return cb(null, eventName, args);
    });
    // socket.on(Event.NewMessage, (msg: any) => {
    //     // console.log("Websocket event received!");
    //     return cb(null, msg);
    // });
};
