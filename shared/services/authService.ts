type JWTToken = string;
type AuthResponse = {
    accessToken?: string;
    errorMessage?: string;
};

export interface IAuthService {
    getToken(): JWTToken;
    removeToken(): void;
    login(email: string, password: string): Promise<AuthResponse>;
    refreshToken(): Promise<AuthResponse>;
    logout(): void;
}

// AuthService uses nextjs functions to login and refreshToken
// They return an accessToken and the refreshToken is stored in an httpOnly, sameSite cookie
// This ensures that the refreshToken is not exposed on the client

export class AuthService implements IAuthService {
    private tokenName;

    constructor(tokenName: string = "mindpulse-auth-token") {
        this.tokenName = tokenName;
    }

    private setToken(token: JWTToken): void {
        localStorage.setItem(this.tokenName, token);
    }

    isAuthenticated(): boolean {
        return !!this.getToken();
    }

    getToken(): JWTToken {
        return typeof window !== "undefined"
            ? (localStorage.getItem(this.tokenName) as JWTToken)
            : "";
    }

    removeToken(): void {
        localStorage.removeItem(this.tokenName);
    }

    private async authFetcher(
        url: string,
        email?: string,
        password?: string
    ): Promise<AuthResponse> {
        try {
            const response = await fetch(url, {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ email, password }),
            });

            if (response.status !== 200) {
                throw new Error(await response.text());
            }

            const { accessToken } = await response.json();
            this.setToken(accessToken);
            return { accessToken };
        } catch (error) {
            console.error(error);
            return {
                errorMessage: error.response
                    ? error.response.data.message
                    : "Connection failed",
            };
        }
    }

    async login(email: string, password: string): Promise<AuthResponse> {
        const url = "/api/login";
        return this.authFetcher(url, email, password);
    }

    refreshToken(): Promise<AuthResponse> {
        const url = "/api/refresh-token";
        return this.authFetcher(url);
    }

    logout(): void {
        throw new Error("Method not implemented.");
    }
}

export const authService = new AuthService();
