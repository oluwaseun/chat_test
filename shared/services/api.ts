import axios, { AxiosResponse } from "axios";
import { apiConfig } from "../../config/api";
import { authService } from "./authService";
import { makeUseAxios } from "axios-hooks";

const api = axios.create({
    baseURL: apiConfig.baseUrl,
    withCredentials: false,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${authService.getToken()}`,
    },
});

api.interceptors.request.use(
    function (config) {
        // Do something before request is sent
        config.headers.Authorization = `Bearer ${authService.getToken()}`;
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    }
);

api.interceptors.response.use(
    function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response;
    },
    async (error: any) => {
        if (didAccessTokenExpire(error.response)) {
            const token = authService.getToken();
            if (token) {
                try {
                    // Get the new access token
                    const {
                        accessToken,
                        errorMessage,
                    } = await authService.refreshToken();
                    if (errorMessage) throw new Error(errorMessage);
                    if (accessToken) {
                        // Retry request
                        error.config.headers["authorization"] = accessToken;
                        return api.request(error.config);
                    }
                } catch (err) {
                    // remove access
                    authService.removeToken();
                    console.log(err);
                }
            }
        }
        return Promise.reject({ ...error });
    }
);

function didAccessTokenExpire(response: AxiosResponse): boolean {
    return response.status === 401;
}

const useAxios = makeUseAxios({
    axios: api,
});
const fetcher = (url: string) => api.get(url).then((res) => res.data.data);

export { api, fetcher, useAxios };
