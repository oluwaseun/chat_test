import {
    ChatMessageData,
    ChatRoomUser,
    ChatUserData,
    DecryptedChatMessageData,
} from "../../../hooks/chat";
import {
    decryptMessage,
    decryptMessageKey,
    encryptMessageKey,
    pemToPublicKey,
} from "../encryptionService";

export async function decryptEncryptedMessages(
    eMessages: ChatMessageData[],
    chatUserId: string,
    keyPair: CryptoKeyPair
): Promise<DecryptedChatMessageData[]> {
    // decrypt message encryption key for user using privateKey
    // decrypt message with decrypted key
    const dMessagePromise = eMessages.map(async (eMessage) => {
        console.log(">>>> eM", eMessage);
        const userData = eMessage.userData.find(
            (user) => user.chatUserId === chatUserId
        );

        if (!userData) throw new Error("user Encryption Data not found");
        const messageKey = await decryptMessageKey(
            keyPair.privateKey,
            userData.eMKey
        );
        const value = await decryptMessage(messageKey, eMessage.eMessage);

        return { ...eMessage, value, fromMe: chatUserId === eMessage.senderId };
    });

    return Promise.all(dMessagePromise);
}

export async function encryptMessageKeyForUsers(
    messageKey: CryptoKey,
    chatRoomUsers: ChatRoomUser[],
    myUserId: string
): Promise<ChatUserData[]> {
    const userDataPromise = chatRoomUsers.map(async (user) => {
        // use user's publicKey to encrypt messageKey
        const publicKey = await pemToPublicKey(user.publicKey);
        const eMKey = await encryptMessageKey(publicKey, messageKey);
        return {
            chatUserId: user.chatUserId,
            isRead: user.chatUserId === myUserId,
            eMKey,
        };
    });
    return Promise.all(userDataPromise);
}
// Socket
// Connect to socket server
// Send a message
