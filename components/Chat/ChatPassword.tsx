import React, { SetStateAction, useState } from "react";
import { useUpdateChatUserKeys } from "../../hooks/chat";
import {
  decryptPrivateKey,
  encryptPrivateKey,
  generateKeyPair,
  pemToPublicKey,
  publicKeyToPem,
} from "../../shared/services/encryptionService";

type ChatPasswordProps = {
  eKeyPair: { ePrivateKey: string; publicKeyPem: string };
  onKeyPairDecryption: (keyPair: CryptoKeyPair) => void;
};

export function ChatPasswordForm({
  eKeyPair: eKeyPair,
  onKeyPairDecryption,
}: ChatPasswordProps) {
  const [decryptError, setDecryptError] = useState("");

  const {
    loading,
    error: updateKeysErros,
    updateChatUserKeys,
  } = useUpdateChatUserKeys();

  const onChatPasswordSubmit = async (chatPassword: string) => {
    try {
      const privateKey = await decryptPrivateKey(
        eKeyPair.ePrivateKey,
        chatPassword
      );
      const publicKey = await pemToPublicKey(eKeyPair.publicKeyPem);
      onKeyPairDecryption({ privateKey, publicKey });
    } catch (decryptionError) {
      setDecryptError(decryptionError);
    }
  };

  const onChatPasswordCreation = async (chatPassword: string) => {
    const keyPair = await generateKeyPair();
    const privateKey = await encryptPrivateKey(
      keyPair.privateKey,
      chatPassword
    );
    const publicKey = await publicKeyToPem(keyPair.publicKey);

    updateChatUserKeys(publicKey, privateKey);
  };

  return (
    <div>
      {decryptError && (
        <h3 className="bg-red-200 text-center p-2 mt-1">
          Incorrect Chat Password entered, please try again.
        </h3>
      )}
      {updateKeysErros && (
        <h3 className="bg-red-200 text-center p-2 mt-1">
          Error Generating Keys, please try again.
        </h3>
      )}
      {loading && "Loadindin Loading... Initializing your encrypted chat"}
      {!loading && !eKeyPair.ePrivateKey && (
        <ChatPasswordCreateForm
          onChatPasswordCreation={onChatPasswordCreation}
        />
      )}

      {eKeyPair.ePrivateKey && (
        <ChatUnlockForm onChatPasswordSubmit={onChatPasswordSubmit} />
      )}
    </div>
  );
}

function ChatPasswordCreateForm({
  onChatPasswordCreation,
}: {
  onChatPasswordCreation: (chatPassword: string) => void;
}) {
  const [chatPassword, setChatPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        onChatPasswordCreation(chatPassword);
      }}
    >
      <p>Please enter a Chat Encryption Password.</p>
      <p>
        Please note that you will have to enter the Chat Password to start a
        chat session
      </p>
      <div>
        <label>
          Chat Password
          <input
            type={showPassword ? "text" : "password"}
            name={chatPassword}
            onChange={(e) => setChatPassword(e.target.value)}
          />
          <i onClick={() => setShowPassword(!showPassword)}>EYE</i>
        </label>
      </div>
      <div>
        <input type="submit" value="Submit" />
      </div>
    </form>
  );
}

function ChatUnlockForm({
  onChatPasswordSubmit,
}: {
  onChatPasswordSubmit: (chatPassword: string) => void;
}) {
  const [chatPassword, setChatPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        onChatPasswordSubmit(chatPassword);
      }}
    >
      <p>Please enter your Chat Password</p>
      <div>
        <label>
          Chat Password
          <input
            type={showPassword ? "text" : "password"}
            name={chatPassword}
            onChange={(e) => setChatPassword(e.target.value)}
          />
          <i onClick={() => setShowPassword(!showPassword)}>EYE</i>
        </label>
      </div>
      <div>
        <input type="submit" value="Open Chat" />
      </div>
    </form>
  );
}
