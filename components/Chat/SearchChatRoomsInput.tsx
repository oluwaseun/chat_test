import { Dispatch, SetStateAction } from "react";

type SearchChatRoomsInputProps = {
  searchQuery: string;
  setSearchQuery: Dispatch<SetStateAction<string>>;
};

export const SearchChatRoomsInput = ({
  searchQuery,
  setSearchQuery,
}: SearchChatRoomsInputProps) => {
  return (
    <div className="bg-blue-200 h-14 p-2 text-blue-500">
      <div className="bg-white p-2 flex items-center rounded-full">
        <label htmlFor="search-rooms" className="mx-2">
          <svg
            className="h-5"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
            />
          </svg>
        </label>
        <input
          className="outline-none"
          id="search-rooms"
          type="text"
          value={searchQuery}
          placeholder="Search or start new search"
          onChange={(e) => setSearchQuery(e.target.value)}
        />
      </div>
    </div>
  );
};
