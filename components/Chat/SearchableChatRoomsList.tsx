import { ChatRoomCard } from "./ChatRoomCard";
import { SearchChatRoomsInput } from "./SearchChatRoomsInput";
import { useState } from "react";
import { ChatRoom } from "../../hooks/chat";

type SideBarProp = {
  rooms: ChatRoom[];
  onChatRoomClick: (room: ChatRoom) => void;
};

export const SearchableRoomsList = ({
  rooms,
  onChatRoomClick,
}: SideBarProp) => {
  const [searchQuery, setSearchQuery] = useState("");

  return (
    <>
      <SearchChatRoomsInput {...{ searchQuery, setSearchQuery }} />
      {/* ChatChaRooms */}
      <div className="bg-blue-200 flex flex-col flex-auto text-gray-800 overflow-y-auto scrollbar-thin scrollbar-track-gray-100 scrollbar-thumb-blue-300 scrollbar-rounded">
        {/* ChatRoom */}
        {rooms
          .filter((cr) => {
            return searchQuery.length
              ? cr.name.toLowerCase().includes(searchQuery.toLowerCase())
              : true;
          })
          .map((chatRoom, idx) => (
            <ChatRoomCard
              key={idx}
              chatRoom={chatRoom}
              onChatRoomClick={onChatRoomClick}
            />
          ))}
      </div>
    </>
  );
};
