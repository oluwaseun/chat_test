import { FunctionComponent } from "react";

type RightSideBarProps = { hide: boolean };

export const RightSideBar: FunctionComponent<RightSideBarProps> = ({
  children,
  hide,
}) => {
  return (
    <div className={`${hide ? "hidden" : ""} bg-blue-300 z-20 w-1/4`}>
      {children}
    </div>
  );
};
