import { FunctionComponent } from "react";

export const LeftSideBar: FunctionComponent = ({ children }: any) => {
  return <div className="w-1/4 flex flex-col">{children}</div>;
};
