import { DecryptedChatMessageData } from "../../hooks/chat";

export const ChatMessage = ({
  message,
}: {
  message: DecryptedChatMessageData;
}) => {
  const sentAt = new Date(message.sentAt);
  return (
    <div className={`${message.fromMe ? "my-msg" : "others-msg"} message`}>
      <p>{message.value}</p>
      <span className="text-xs ml-6 self-end text-gray-600">
        {sentAt.toTimeString().slice(0, 5)}
      </span>
    </div>
  );
};
