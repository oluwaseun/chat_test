import { ChatRoom } from "../../hooks/chat";

type ChatRoomCardProp = {
  chatRoom: ChatRoom;
  onChatRoomClick: (room: ChatRoom) => void;
};

export const ChatRoomCard = ({
  chatRoom,
  onChatRoomClick: onChatRoomClick,
}: ChatRoomCardProp) => {
  return (
    <div
      className="bg-blue-100 flex p-4 mx-1 mt-1 rounded-lg shadow cursor-pointer"
      onClick={(e) => onChatRoomClick(chatRoom)}
    >
      <div className="w-full">
        <div className="flex justify-between items-center">
          <span>{chatRoom.name}</span>
          {/* <span className="text-xs text-gray-800">
            {chatRoom.lastMessageTime.toTimeString().slice(0, 5)}
          </span> */}
        </div>
        {/* <div className="text-sm text-gray-800">{chatRoom.lastMessage.}</div> */}
      </div>
    </div>
  );
};
