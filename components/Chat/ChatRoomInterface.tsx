import React, { useEffect, useState } from "react";
import {
  ChatMessageData,
  ChatRoom,
  ChatUser,
  DecryptedChatMessageData,
  useChatRoomMessages,
} from "../../hooks/chat";
import {
  decryptEncryptedMessages,
  encryptMessageKeyForUsers,
} from "../../shared/services/chatService";
import {
  encryptMessage,
  generateMessageEncryptionKey,
} from "../../shared/services/encryptionService";
import {
  ChatEvent,
  disconnectSocket,
  initiateSocket,
  sendMessage,
  subscribeToChatSocket,
} from "../../shared/services/socket";
import { ChatFooter } from "./ChatFooter";
import { ChatMessage } from "./ChatMessage";

export const ChatRoomInterface = ({
  keyPair,
  chatRoom,
  chatUser,
  onContactClick,
}: {
  keyPair: CryptoKeyPair;
  chatRoom: ChatRoom;
  chatUser: Omit<ChatUser, "rooms">;
  onContactClick: () => void;
}) => {
  const { eMessages, loading, error } = useChatRoomMessages(chatRoom.roomId);
  const [messages, setMessages] = useState<DecryptedChatMessageData[]>([]);
  // Let's do unoptimized rendering of messages first
  // TODO: Optimize rendering of large number of messages

  useEffect(() => {
    if (eMessages)
      decryptEncryptedMessages(
        eMessages,
        chatUser.chatUserId,
        keyPair
      ).then((dMessages) => setMessages(dMessages));
  }, [eMessages]);

  useEffect(() => {
    initiateSocket(chatRoom.roomId, chatUser.chatUserId);
    subscribeToChatSocket((err: Error | null, event: ChatEvent, data: any) => {
	  if (err) return;
	  console.log(data)
      switch (event) {
        case ChatEvent.NewMessage:
          decryptEncryptedMessages([data], chatUser.chatUserId, keyPair).then(
            (dMessages) => {
              const newMessages = [...dMessages, ...messages];
              setMessages(newMessages);
            }
          );
          break;
        case ChatEvent.UserTyping:
          // Set status to typing
          break;
        case ChatEvent.UserStoppedTyping:
          break;
        case ChatEvent.UserJoined:
          break;
        case ChatEvent.LeftRoom:
          break;
        case ChatEvent.MessageRead:
          break;
        default:
          break;
      }
    });
    return () => {
      disconnectSocket();
    };
  }, [chatRoom, messages]);

  const addNewMessage = async (messageText: string) => {
    const messageKey = await generateMessageEncryptionKey();
    const eMessage = await encryptMessage(messageText, messageKey);
    const userData = await encryptMessageKeyForUsers(
      messageKey,
      chatRoom.users,
      chatUser.chatUserId
    );

    const newMessage: ChatMessageData = {
      id: Math.random().toString(36).substring(10),
      sentAt: new Date().toISOString(),
      chatRoomId: chatRoom.roomId,
      eMessage,
      senderId: chatUser.chatUserId,
      userData,
    };
    setMessages([
      { ...newMessage, value: messageText, fromMe: true },
      ...messages,
    ]);

    // Send to API through socket
    sendMessage(newMessage);
  };

  return (
    <div className="bg-blue-300 w-2/4 flex flex-col flex-grow">
      {chatRoom.roomId ? (
        <>
          {/* ChatHeader */}
          <div className="bg-blue-200 flex px-4 py-2 justify-between items-center">
            <div
              className="flex cursor-pointer"
              onClick={() => onContactClick()}
            >
              {/* <div className="thumbnail-container mr-4">
                <img
                  className="thumbnail-image"
                  src={channel.displayPicture}
                  alt={`${channel.name} profile picture`}
                />
              </div> */}
              {/* Contact */}
              <div className="flex-grow text-gray-800">
                <div>{chatRoom.name}</div>
                <div className="text-sm">{"online"}</div>
              </div>
            </div>
            {/* ChatHeaderButtonsWrapper */}
            <div className="flex text-blue-500">
              {/* SearchMessagesButton */}
              <span>
                <button className="ml-2 p-2 focus:outline-none">
                  <svg
                    className="h-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                </button>
              </span>
              {/* ChatMenu */}
              <span>
                <button className="ml-2 p-2 focus:outline-none">
                  <svg
                    className="h-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="curren        console.log('>>>> token', authService.getToken());
					tColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"
                    />
                  </svg>
                </button>
              </span>
            </div>
          </div>
          {/* ChatWindow */}
          <div className="bg-blue-100 flex-grow px-12 overflow-y-auto scrollbar-thin scrollbar-track-gray-100 scrollbar-thumb-blue-300 flex flex-col-reverse">
            {/* ChatMessage */}
            {messages.map((message) => {
              return <ChatMessage key={message.id} message={message} />;
            })}
          </div>
          {/* ChatFooter */}
          <ChatFooter onNewMessage={addNewMessage} />
        </>
      ) : (
        <p>Select a user to chat with</p>
      )}
    </div>
  );
};
