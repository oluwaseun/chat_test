export const SideBarHeader = () => {
  return (
    <div className="bg-blue-200 flex px-4 py-2 justify-between items-center ">
      {/* ProfileImage */}
      <div className="thumbnail-container mr-4">
        <img
          className="thumbnail-image"
          src={"/images/profile.jpg"}
          alt={`user profile image`}
        />
      </div>
      {/* HeaderButtonsWrapper */}
      <div className="flex text-blue-500">
        {/* <span>StatusButton</span> */}
        {/* NewChatButton */}
        <span>
          <button className="ml-2 p-2 focus:outline-none">
            <svg
              className="h-6"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
              />
            </svg>
          </button>
        </span>
        {/* SideBarMenuButton */}
        <span>
          <button className="ml-2 p-2 focus:outline-none">
            <svg
              className="h-6"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z"
              />
            </svg>
          </button>
        </span>
      </div>
    </div>
  );
};
