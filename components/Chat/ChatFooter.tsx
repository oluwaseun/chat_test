import React, { useState } from "react";

export const ChatFooter = ({ onNewMessage }: { onNewMessage: (message: string) => Promise<void> }) => {
  const [messageText, setMessageText] = useState("");
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    onNewMessage(messageText);
    setMessageText("");
  };

  return (
    <div>
      <div className="bg-blue-300 h-14 p-2 flex items-center text-blue-500">
        <div className="flex">
          {/* EmojiButton */}
          <span>
            <button className="p-2 focus:outline-none">
              <svg
                className="h-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
            </button>
          </span>
          {/* AttachButton */}
          <span>
            <button className="mx-2 p-2 focus:outline-none">
              <svg
                className="h-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M15.172 7l-6.586 6.586a2 2 0 102.828 2.828l6.414-6.586a4 4 0 00-5.656-5.656l-6.415 6.585a6 6 0 108.486 8.486L20.5 13"
                />
              </svg>
            </button>
          </span>
        </div>
        <form
          onSubmit={handleSubmit}
          className="bg-white p-2 flex flex-grow items-center rounded-full"
        >
          <label htmlFor="message-input" className="mx-2">
            <span className="sr-only">Type a messsage</span>
          </label>
          <input
            className="outline-none w-full"
            id="message-input"
            type="text"
            autoComplete="off"
            placeholder="Type a message"
            value={messageText}
            onChange={(e) => setMessageText(e.target.value)}
          />
          <input type="submit" value="" />
        </form>
        {/* VoiceMessageButton */}
        <span>
          <button className="mx-2 p-2 focus:outline-none">
            <svg
              className="h-6"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M19 11a7 7 0 01-7 7m0 0a7 7 0 01-7-7m7 7v4m0 0H8m4 0h4m-4-8a3 3 0 01-3-3V5a3 3 0 116 0v6a3 3 0 01-3 3z"
              />
            </svg>
          </button>
        </span>
      </div>
    </div>
  );
};
