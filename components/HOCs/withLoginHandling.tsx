import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { authService } from "../../shared/services/authService";

export function withLoginHandling<T>(WrappedComponent: React.ComponentType<T>) {
  // Try to create a nice displayName for React Dev Tools.
  const displayName =
    WrappedComponent.displayName || WrappedComponent.name || "Component";

  const ComponentWithLoginHandling = (props: T) => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const router = useRouter();

    const isAuthenticated = authService.isAuthenticated();

    const handleLoggedIn = () => {
      const { redirect } = router.query;
      console.log("redirect", redirect);
      redirect ? router.back() : router.push("/dashboard");
    };

    useEffect(() => {
      if (isAuthenticated) {
        handleLoggedIn();
      }
    });

	const handleLogin = async (email: string, password: string) => {
		setLoading(true);
		const { errorMessage } = await authService.login(email, password);
		if (errorMessage) {
		  setError(errorMessage);
		}
		handleLoggedIn();
		setLoading(false);
	  };

    const loginHandlingProps = {
      handleLogin,
      loading,
      error,
    };

    // props comes afterwards so the can override the default ones.
    return <WrappedComponent {...loginHandlingProps} {...props} />;
  };

  ComponentWithLoginHandling.displayName = `withLoginHandling(${displayName})`;

  return ComponentWithLoginHandling;
}
