import { useRouter } from "next/router";
import { ReactNode, useEffect } from "react";
import { useCurrentUser } from "../hooks/users";
import { authService } from "../shared/services/authService";

type LayoutProps = {
  children: ReactNode;
};

export default function Layout({ children }: LayoutProps) {
  const { user, error: isError, loading: isLoading } = useCurrentUser();
  const router = useRouter();

  const handleRedirectToLogin = () => {
    router.push("/login?redirect=true");
  };

  useEffect(() => {
    if (isError && !authService.isAuthenticated()) {
      handleRedirectToLogin();
    }
  });

  return (
    <div>
      {isError && <p>An Error has occured please try again</p>}
      {isLoading && <p>Loading...</p>}
      {user && children}
    </div>
  );
}
// type LayoutProps = {
//   children: ({ user }: { user: UserProfile }) => JSX.Element;
// }
// {user && children({ user })}
